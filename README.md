# Dotfiles

My personal dotfiles for `vim`, `i3`/`i3-gaps`, and `polybar`. It is mostly for my own personal use, but please feel free to fork and make your own.

![https://i.imgur.com/WRYc4Gk.jpg](https://i.imgur.com/WRYc4Gk.jpg)

## Tools/Applications:

- Distro: Arch Linux
- Terminal: Gnome Terminal
- Editor: vim
- Color Scheme: [Base16](https://github.com/chriskempson/base16) (default-dark)
- Window Manager: i3, i3-gaps
- Menu Bar: polybar
- File Manager: [ranger](https://github.com/ranger/ranger)
- Wallpaper: [By Aleks Dahlberg](https://unsplash.com/photos/b7DzEp2lsRI)
